from django.contrib import admin
from .models import ExpenseCategory
from .models import Account
from .models import Receipt


# Register your models here.
@admin.register(ExpenseCategory)
class ExpensesAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )


@admin.register(Account)
class AccountsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
    )


@admin.register(Receipt)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        )
